
###########################################################################
###################### M. guttatus populations PCA ########################
###########################################################################

################### Daniele Da Re, Mario Vallejo-Marin ####################

#set wd and load libraries 
setwd("/home/ddare/Documents/DanieleDaRe/UCL/02_Codes/GitLab_repo_DDR/mimulus_enm/")
library(ecospat)
library(FactoMineR)

#load occurences and covariates
gut_uk <- na.omit(read.csv("TrainingPoints/data_gut_eu_out.csv"))
gut_na <- na.omit(read.csv("TrainingPoints/data_gut_na_out.csv"))

#subset
gut_uk<-gut_uk[which(gut_uk$pa==1 & gut_uk$X<= 1 & gut_uk$Y>= 49 & gut_uk$Y<= 60.8), ]
plot(gut_uk$X, gut_uk$Y)
gut_uk$ID<-as.factor(rep("UK", nrow(gut_uk)))

gut_na_north<-gut_na[which(gut_na$pa==1 & gut_na$Y>= 51.9), ]
plot(gut_na_north$X, gut_na_north$Y)
gut_na_north$ID<-as.factor(rep("NAN", nrow(gut_na_north)))

gut_na_south<-gut_na[which(gut_na$pa==1 & gut_na$Y<= 51.9), ]
plot(gut_na_south$X, gut_na_south$Y)
gut_na_south$ID<-as.factor(rep("NAS", nrow(gut_na_south)))

#prepare df for PCA
pca_data<-rbind(gut_uk[,c(3:10,12)], gut_na_north[,c(3:10,12)], gut_na_south[,c(3:10,12)])
names(pca_data)
str(pca_data)

#PCA
gut.pca <- PCA(pca_data, quali.sup=9,scale.unit=TRUE)

plot(gut.pca)
plot(gut.pca, habillage = 9, 
     col.hab = c("orange", "black", "purple"), 
     cex=0.8, 
     label = "none")

plot(gut.pca, choix="var")

str(gut.pca)
gut.pca$call$X$ID
gut.pca$var$coord
                
                pdf(file="/home/ddare/Desktop/gut_pop_climPCA.pdf", height=8, width=12)
                par(mfrow=c(2,1))
                plot(gut.pca, habillage = 9, 
                     col.hab = c("orange", "black", "purple"), 
                     cex=0.8, 
                     label = "none")
                plot(gut.pca, choix="var")
                dev.off()