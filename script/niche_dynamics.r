#######################################################
################## Niches Dynamics ####################
#######################################################

######### Daniele Da Re, Mario Vallejo-Marin #########

#set wd and load libraries 
setwd("/home/ddare/Documents/DanieleDaRe/UCL/02_Codes/GitLab_repo_DDR/mimulus_enm/")
library(ecospat)

#load occurences and covariates
gut_eu <- na.omit(read.csv("TrainingPoints/data_gut_eu_out.csv"))
gut_na <- na.omit(read.csv("TrainingPoints/data_gut_na_out.csv"))
gut_nz <- na.omit(read.csv("TrainingPoints/data_gut_nz_out.csv"))

lut_eu <- na.omit(read.csv("TrainingPoints/data_lut_eu_out.csv"))
lut_sa <- na.omit(read.csv("TrainingPoints/data_lut_sa_out.csv"))

rob_eu <- na.omit(read.csv("TrainingPoints/data_rob_eu_out.csv"))

#Define native and invasive niches
nat<-lut_eu
inv<-gut_eu

nat<- transform(nat, bio1 = as.numeric(bio1))
inv<- transform(inv, bio1 = as.numeric(bio1))
nat[, 3:11] <- sapply(nat[, 3:11], as.numeric)
inv[, 3:11] <- sapply(inv[, 3:11], as.numeric)

#pca-env
pca.env<-dudi.pca(rbind(nat,inv)[,3:10],scannf=FALSE,nf=2)

#Predict the scores on the axes
scores.globclim<-pca.env$li #   PCA scores  for the whole   study   area

scores.sp.nat<-suprow(pca.env,nat[which(nat[,11]==1),3:10])$li #PCA scores  for the species native  distribution
scores.sp.inv<-suprow(pca.env,inv[which(inv[,11]==1),3:10])$li # PCA    scores  for the species invasive    distribution

scores.clim.nat<-suprow(pca.env,nat[,3:10])$li #    PCA scores  for the whole   native  study   area
scores.clim.inv<-suprow(pca.env,inv[,3:10])$li #    PCA scores  for the whole   invaded study area

#Calculate the Occurrence Densities Grid

#   For the species in  the native  range   (North  America)
grid.clim.nat<-ecospat.grid.clim.dyn(glob=scores.globclim, glob1=scores.clim.nat, sp=scores.sp.nat, R=100, th.sp=0)
ecospat.plot.niche(grid.clim.nat)

#   For the species in  the invaded range   (Europe)
grid.clim.inv<-ecospat.grid.clim.dyn(glob=scores.globclim, glob1=scores.clim.inv, sp=scores.sp.inv, R=100, th.sp=0)
ecospat.plot.niche(grid.clim.inv)

#niche overlap
ecospat.niche.overlap (grid.clim.nat, grid.clim.inv, cor=TRUE) 

#niche equivalency Test
eq.test<-ecospat.niche.equivalency.test(grid.clim.nat,  grid.clim.inv, rep=100, alternative = "greater") #testing niche conservatism 
summary(eq.test$sim)
eq.test$obs$D
eq.test$p.D
ecospat.plot.overlap.test(eq.test, "D", "Equivalency - Niche conservation HP")

#Niche Similarity Test 
sim.test<-ecospat.niche.similarity.test(grid.clim.nat,  grid.clim.inv, rep=100, alternative = "greater",  rand.type=1) #testing niche conservatism 
summary(sim.test$sim)
sim.test$obs$D
sim.test$p.D
ecospat.plot.overlap.test(sim.test, "D", "Similarity - Niche conservation HP")

#niche equivalency Test
eq.test<-ecospat.niche.equivalency.test(grid.clim.nat,  grid.clim.inv, rep=100, alternative = "lower") #testing niche shift 
summary(eq.test$sim)
eq.test$obs$D
eq.test$p.D
ecospat.plot.overlap.test(eq.test, "D", "Equivalency - Niche shift HP")

#Niche Similarity Test 
sim.test<-ecospat.niche.similarity.test(grid.clim.nat,  grid.clim.inv, rep=100, alternative = "lower",  rand.type=1) #testing niche shift 
summary(sim.test$sim)
sim.test$obs$D
sim.test$p.D
ecospat.plot.overlap.test(sim.test, "D", "Similarity - Niche shift HP")

#niche dynamics
x<-ecospat.niche.dyn.index (grid.clim.nat, grid.clim.inv, intersection=0.1)#e calculated each index using the 90th percentile of the available environmental conditions common to both ranges (Di Cola et al., 2017). 
x$dynamic.index.w
#the niche dynamics analysis shows that this niche difference is due to the species' ability to expand into novel climates in the invaded range (niche expansion = 16%) and to the fact that the species has not (yet) colonized all the climate conditions of the native niche (niche unfilling = 33%)

#Visualizing    niche   categories, niche   dynamics    and climate analogy between ranges  
pdf(file="/home/ddare/Desktop/lutEU_gutEU.pdf", height=8, width=12)
ecospat.plot.niche.dyn(grid.clim.nat, grid.clim.inv, quant=0.1, interest=2, name.axis1="PC1", name.axis2="PC2", colZ1="lightblue", colZ2="orange",colz1="lightblue", colz2="orange")  # interest=2, plot invasive density.
ecospat.shift.centroids(scores.sp.nat, scores.sp.inv, scores.clim.nat, scores.clim.inv)
dev.off()