#######################################################
################## Climatic analysis #################
#######################################################

######### Daniele Da Re, Mario Vallejo-Marin #########

#set wd and load libraries 
setwd("/home/ddare/Documents/DanieleDaRe/UCL/02_Codes/GitLab_repo_DDR/mimulus_enm/")
library(raster)
library(dismo)
library(ecospat)

#load
gut_eu <- na.omit(read.csv("TrainingPoints/data_gut_eu_out.csv"))
gut_eu$reg<-rep("EU", nrow(gut_eu))
gut_na <- na.omit(read.csv("TrainingPoints/data_gut_na_out.csv"))
gut_na$reg<-rep("NA", nrow(gut_na))
gut_nz <- na.omit(read.csv("TrainingPoints/data_gut_nz_out.csv"))
gut_nz$reg<-rep("NZ", nrow(gut_nz))

lut_eu <- na.omit(read.csv("TrainingPoints/data_lut_eu_out.csv"))
lut_eu$reg<-rep("EU", nrow(lut_eu))

lut_sa <- na.omit(read.csv("TrainingPoints/data_lut_sa_out.csv"))
lut_sa$reg<-rep("SA", nrow(lut_sa))
rob_eu <- na.omit(read.csv("TrainingPoints/data_rob_eu_out.csv"))
rob_eu$reg<-rep("EU", nrow(rob_eu))

gutNAEU<- rbind(gut_na, gut_eu)
gutNANZ<- rbind(gut_na, gut_nz)
gutEUNZ<- rbind(gut_eu, gut_nz)

lutSAEU<- rbind(lut_sa, lut_eu)
gutlutNASA<- rbind(gut_na, lut_sa)
  
#M. guttatus PCA
tmp1<- dudi.pca(gutNAEU[,3:10], nf=2, scannf=F)
tmp2<- data.frame(cbind(gutNAEU,tmp1$li))
cols<- rep("#3090C733",nrow(gutNAEU))
cols[gutNAEU$reg == "EU"]<- "#9F000F4D"
  
tmp3<- dudi.pca(gutNANZ[,3:10], nf=2, scannf=F)
tmp4<- data.frame(cbind(gutNANZ,tmp3$li))
cols<- rep("#3090C733",nrow(gutNANZ))
cols[gutNANZ$reg == "NZ"]<- "#9F000F4D" #red color
  
tmp5<- dudi.pca(gutEUNZ[,3:10], nf=2, scannf=F)
tmp6<- data.frame(cbind(gutEUNZ,tmp5$li))
cols<- rep("#3090C733",nrow(gutEUNZ))
cols[gutEUNZ$reg == "NZ"]<- "#9F000F4D" #red color
  
pdf(file="/home/ddare/Desktop/gut_climPCA.pdf", height=8, width=12)
  par(mfrow=c(1,3))
  plot(jitter(tmp2$Axis1,amount=.3),jitter(tmp2$Axis2, amount=.3),
       col=cols, pch=16,cex=.5,
       xlab="PCA- Axis 1", ylab="PCA- Axis 2", 
       main= "North America and Europe")
  
  plot(jitter(tmp4$Axis1,amount=.3),jitter(tmp4$Axis2, amount=.3),
       col=cols, pch=16,cex=.6,
       xlab="PCA- Axis 1", ylab="PCA- Axis 2", 
       main= "North America and New Zealand")
  
  plot(jitter(tmp6$Axis1,amount=.3),jitter(tmp6$Axis2, amount=.3),
       col=cols, pch=16,cex=.5,
       xlab="PCA- Axis 1", ylab="PCA- Axis 2", 
       main= "Europe and New Zealand")
  par(mfrow=c(1,1))
dev.off()
  
#M. luteus PCA
tmp7<- dudi.pca(lutSAEU[,3:10], nf=2, scannf=F)
tmp8<- data.frame(cbind(lutSAEU,tmp7$li))
cols<- rep("#3090C733",nrow(lutSAEU))
cols[lutSAEU$reg == "EU"]<- "#9F000F4D" #red color
  
tmp9<- dudi.pca(gutlutNASA[,3:10], nf=2, scannf=F)
tmp10<- data.frame(cbind(gutlutNASA,tmp9$li))
cols<- rep("#3090C733",nrow(gutlutNASA))
cols[gutlutNASA$reg == "SA"]<- "#9F000F4D" #red color
  
pdf(file="/home/ddare/Desktop/lut_climPCA.pdf", height=8, width=12)
  par(mfrow=c(1,2))
  plot(jitter(tmp8$Axis1,amount=.3),jitter(tmp8$Axis2, amount=.3),
       col=cols, pch=16,cex=.6,
       xlab="PCA- Axis 1", ylab="PCA- Axis 2", 
       main= "South America and Europe")
  
  plot(jitter(tmp10$Axis1,amount=.3),jitter(tmp10$Axis2, amount=.3),
       col=cols, pch=16,cex=.5,
       xlab="PCA- Axis 1", ylab="PCA- Axis 2", 
       main= "North America and South America")
  par(mfrow=c(1,1))
dev.off()
